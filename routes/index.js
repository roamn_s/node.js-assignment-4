const express = require('express');

const controllers = require('../controllers');

const router = express.Router();

router.get('/', controllers.getIndex);

router.post('/', controllers.postIndex);

router.get('/users', controllers.getUsers);

router.post('/users', controllers.postDeleteUsers);

router.get('*', controllers.get404);

module.exports = router;
