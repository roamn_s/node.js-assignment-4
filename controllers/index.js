const models = require('../models');

module.exports.getIndex = (req, res) => {
  res.render('../views/index', {
    pageTitle: 'Index page',
    path: '/',
    usersNumber: models.users.getNumber(),
  });
};

module.exports.postIndex = (req, res) => {
  models.users.addItem(req.body.name);
  res.redirect('/');
};

module.exports.getUsers = (req, res) => {
  const usersList = models.users.getList();
  res.render('../views/users', {
    pageTitle: 'Users list',
    path: '/users',
    usersList,
    usersNumber: models.users.getNumber(),
  });
};

module.exports.postDeleteUsers = (req, res) => {
  models.users.deleteUsers();
  res.redirect('/users');
};

module.exports.get404 = (req, res) => {
  res.render('../views/404', {
    pageTitle: 'Oops! Page not found.',
    path: '/404',
    usersNumber: models.users.getNumber(),
  });
};
