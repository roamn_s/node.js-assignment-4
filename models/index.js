class Users {
  constructor() {
    this.users = [];
  }

  addItem(userName = '') {
    if (!userName) {
      return Error('userName is empty');
    }
    this.users.push(userName);
  }

  getList() {
    return this.users;
  }

  getNumber() {
    return this.users.length;
  }

  deleteUsers() {
    return this.users = [];
  }
}

module.exports.users = new Users();
