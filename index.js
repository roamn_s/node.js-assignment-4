const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes');

const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('static'))
app.use(router);

app.listen(3000, () => {
  console.log('App listening on port 3000');
});
